<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="shared/header.jsp">
    <c:param name="pageName" value="orderList"/>
</c:import>

<html>
<head>
    <title>Zamówienia</title>
</head>
<body>

<div class="container text-center">

    <form:form id="searchForm" modelAttribute="searchCommand">
        <div class="mb-3">
            <div class="row row-space ml-auto mr-auto">
                <div class="col-md-3 mb-3 ml-auto">
                    <label for="status">Wyświetl zamówienia</label>
                    <div class="input-group">
                        <form:select path="status" cssClass="form-control">
                            <form:option value="0">Wszystkie</form:option>
                            <form:options items="${statuses}" itemLabel="name" itemValue="id"/>
                        </form:select>
                    </div>
                </div>
                <div class="col-md-3 mb-3 mr-auto">
                    <button type="submit" class="btn btn-primary btn-md btn-block btn-raised mr-auto">
                        Wyświetl
                    </button>
                </div>
            </div>
        </div>
    </form:form>

    <c:choose>
        <c:when test="${!empty orders.content}">
            <table class="table table-hover table-striped table-sm">
                <thead>
                <tr>
                    <th scope="col">Data złożenia zamówienia</th>
                    <th scope="col">Cena całkowita</th>
                    <th scope="col">Status</th>
                    <th scope="col" class="text-center" colspan="2">Opcje</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${orders.content}" var="order">
                    <tr>
                        <th scope="row">
                            <fmt:formatDate value="${order.date}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </th>
                        <td>
                                ${order.totalValue} PLN
                        </td>
                        <td>
                                ${order.status.name}
                        </td>
                        <td>
                            <a href="/orders/${order.id}" class="btn btn-primary btn-sm btn-block btn-raised">Więcej</a>
                            <c:if test="${order.status.id == 1}">
                                <a href="/orders/cancel/${order.id}"
                                   class="btn btn-danger btn-sm btn-block btn-raised">Anuluj</a>
                            </c:if>
                    <%--<c:if test="${order.status.id == 2}">
                         <a href="/orders/pdf/${order.id}" target="_blank"
                            class="btn btn-success btn-sm btn-block btn-raised">PDF</a>
                     </c:if> --%>
                 </td>
             </tr>
         </c:forEach>
         </tbody>
     </table>

     <c:set var="page" value="${orders}" scope="request"/>
     <c:set var="mainUrl" value="orders" scope="request"/>
     <c:import url="shared/pagination.jsp"/>

 </c:when>
 <c:otherwise>
     <h1>Brak zamówień</h1>
 </c:otherwise>
</c:choose>
</div>
</body>
</html>

<c:import url="shared/footer.jsp"/>
