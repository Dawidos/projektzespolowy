<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="shared/header.jsp">
    <c:param name="pageName" value="gameList"/>
</c:import>

<html>
<head>
    <title>Lista gier</title>
</head>
<body>

<div class="container text-center">

    <form:form id="searchForm" modelAttribute="searchCommand">
        <div class="mb-3">
            <div class="row row-space col-sm-12">
                <div class="col-md-5 mb-3">
                    <label for="phrase">Nazwa</label>
                    <form:errors path="phrase" cssClass="error text-danger" element="div"/>
                    <div class="input-group">
                        <form:input path="phrase" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
                    </div>
                </div>
                <div class="col-md-2 mb-3">
                    <label for="priceMin">Cena min</label>
                    <form:errors path="priceMin" cssClass="error text-danger" element="div"/>
                    <div class="input-group">
                        <form:input path="priceMin" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="priceMax">Cena max</label>
                    <form:errors path="priceMax" cssClass="error text-danger" element="div"/>
                    <div class="input-group">
                        <form:input path="priceMax" cssClass="form-control" cssErrorClass="form-control is-invalid"/>
                    </div>
                </div>
                <div class="col-md-3">
                    <button type="submit" class="btn btn-primary btn-md btn-block btn-raised">Szukaj</button>
                </div>
            </div>
        </div>
    </form:form>
    <hr>
    <c:choose>
        <c:when test="${!empty gameListPage.content}">
            <c:forEach var="store" items="${gameListPage.content}">
                <div class="bg-light mb-5 text-dark pr-3 pl-3">
                    <div class="row row-space">
                        <div class="col-md-12">
                            <p>
                            <h3><u><a class="btn btn-lg btn-primary btn-raised btn-block"
                                      href="/gameList/${store.game.id}">${store.game.name}</a></u></h3></p>
                        </div>
                    </div>
                    <div class="row row-space">
                        <div class="col-md-4 mb-3">
                            <c:choose>
                                <c:when test="${!empty store.game.coverURI}">
                                    <img class="col-md-8"
                                         src="${store.game.coverURI}"
                                         alt="Brak zdjęcia"/>
                                </c:when>
                                <c:otherwise>
                                    Brak zdjęcia
                                </c:otherwise>
                            </c:choose>
                        </div>
                        <div class="col-md-8 mb-3">
                            <div class="row row-space">
                                <div class="col-md-12">
                                    <p>${store.game.descriptionShort}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-space">
                        <c:choose>
                            <c:when test="${store.quantity > 0}">
                                <div class="col-md-12">
                                    <a class="btn btn-success btn-lg btn-raised btn-block"
                                       href="/shoppingCart/add/${store.game.id}">Dodaj
                                        do
                                        koszyka (${store.game.price} PLN)</a>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-md-12">
                                    <p>Brak w sklepie</p>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </c:forEach>

            <c:set var="page" value="${gameListPage}" scope="request"/>
            <c:set var="mainUrl" value="gameList" scope="request"/>
            <c:import url="shared/pagination.jsp"/>

        </c:when>
        <c:otherwise>
            <h1>Brak danych</h1>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>

<c:import url="shared/footer.jsp"/>
