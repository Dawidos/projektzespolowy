<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>

<body>
<BR><BR>
<footer class="footer bg-light fixed-bottom">
    <div class="container text-center">
        <div class="row">
            <div class="col-md-4 ml-auto mr-auto">
                Dawid Pniewski & Krzysztof Osiak - Sklep z grami
            </div>
            <security:authorize access="hasRole('ADMIN')">
                <div class="col-md-4 ml-auto mr-auto">
                    <div class="dropup show">
                        <a class="text-danger dropdown-toggle" href="#" role="button"
                           id="dropdownMenuLinkForAdmin"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <strong>Administrator</strong>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLinkForAdmin">
                            <a class="dropdown-item ${param['pageName'] == 'accountManagement' ? ' active':''}"
                               href="/adminPanel/accountManagement">Zarządzanie kontami</a>
                            <a class="dropdown-item ${param['pageName'] == 'gameCategoriesManagement' ? ' active':''}"
                               href="/adminPanel/gameCategoriesManagement">Zarządzanie kategoriami gier</a>
                        </div>
                    </div>
                </div>

            </security:authorize>
            <security:authorize access="hasRole('EMPLOYEE')">
                <div class="col-md-4 ml-auto mr-auto">
                    <div class="dropup show">
                        <a class="text-danger dropdown-toggle" href="#" role="button"
                           id="dropdownMenuLinkForEmployee"
                           data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <strong>Pracownik</strong>
                        </a>

                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLinkForEmployee">
                            <a class="dropdown-item ${param['pageName'] == 'gameManagement' ? ' active':''}"
                               href="/employeePanel/gameManagement">Zarządzanie grami</a>
                            <a class="dropdown-item ${param['pageName'] == 'orderManagement' ? ' active':''}"
                               href="/employeePanel/orderManagement">Zarządzanie zamówieniami</a>
                        </div>
                    </div>
                </div>
            </security:authorize>
        </div>
    </div>
</footer>
</body>
</html>

