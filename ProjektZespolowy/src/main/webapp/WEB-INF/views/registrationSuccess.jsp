<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<c:import url="shared/header.jsp">
    <c:param name="pageName" value="registrationSuccess"/>
</c:import>
<head>
    <title>Title</title>
</head>
<body>
<main role="main" class="container text-center">
    <div class="starter-template">
        <h1>${title}</h1>
        <p class="lead">
            ${message}
        </p>
    </div>
</main>
</body>
<c:import url="shared/footer.jsp"/>
</html>
