<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="shared/header.jsp">
    <c:param name="pageName" value="shoppingCart"/>
</c:import>

<html>
<head>
    <title>Koszyk</title>
</head>
<body>

<div class="text-center container-fluid">
    <c:choose>
        <c:when test="${!empty cart}">
            <h1>Koszyk</h1>
            <table class="table table-hover table-striped table-sm">
                <thead>
                <th>Okładka</th>
                <th>Nazwa</th>
                <th>Ilość</th>
                <th>Cena gry</th>
                <th>Cena łączna</th>
                <th>Opcje</th>
                </thead>
                <tbody>
                <c:forEach var="item" items="${cart}">
                    <tr>
                        <td>
                            <c:choose>
                                <c:when test="${!empty item.game.coverURI}">
                                    <img style="width: 15%;" src="${item.game.coverURI}" alt="Brak zdjęcia"/>
                                </c:when>
                                <c:otherwise>
                                    Brak zdjęcia
                                </c:otherwise>
                            </c:choose>
                        </td>
                        <td>
                            <u><a class="btn btn-md btn-block"
                                  href="/gameList/${item.game.id}">${item.game.name}</a></u>
                        </td>
                        <td>
                            <a class="btn btn-warning btn-sm btn-raised"
                               href="/shoppingCart/decrease/${item.game.id}">-</a>
                                ${item.quantity}
                            <a class="btn btn-primary btn-sm btn-raised"
                               href="/shoppingCart/add/${item.game.id}">+</a>
                        </td>
                        <td>
                                ${item.game.price} PLN
                        </td>
                        <td>
                                ${item.game.price * item.quantity} PLN
                        </td>
                        <td>
                            <a class="btn btn-danger btn-sm btn-block btn-raised" href="/shoppingCart/remove/${item.game.id}">Usuń
                                z koszyka</a>
                        </td>
                    </tr>
                </c:forEach>
                <tr>
                    <td colspan="6"><a class="btn btn-success btn-lg btn-block btn-raised" href="/shoppingCart/buy">Kup (${totalValue} PLN)</a></td>
                </tr>
                </tbody>
            </table>
        </c:when>
        <c:otherwise>
            <h1>Koszyk jest pusty</h1>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>

<c:import url="shared/footer.jsp"/>
