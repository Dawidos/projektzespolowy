<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Strona główna</title>
</head>
<body>

<c:import url="shared/header.jsp">
    <c:param name="pageName" value="index"/>
</c:import>
<main role="main" class="container text-center">
    <div class="starter-template">
        <h1>Projekt zespołowy</h1>
        <p class="lead">
            <strong>Dawid Pniewski & Krzysztof Osiak - Sklep z grami</strong>
        </p>
    </div>
</main>
<c:import url="shared/footer.jsp"/>
</body>
</html>
