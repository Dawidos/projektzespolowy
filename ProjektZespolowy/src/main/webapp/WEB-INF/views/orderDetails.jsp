<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="shared/header.jsp">
    <c:param name="pageName" value="orderDetails"/>
</c:import>

<html>
<head>
    <title>Szczegóły zamówienia</title>
</head>
<body>

<div class="text-center container-fluid">

    <h1>Szczegóły zamówienia</h1>

    <table class="table table-hover table-striped table-sm">
        <thead>
        <th>Okładka</th>
        <th>Nazwa</th>
        <th>Ilość</th>
        <th>Cena gry</th>
        <th>Cena łączna</th>
        </thead>
        <tbody>
        <c:forEach var="item" items="${order.orderDetails}">
            <tr>
                <td>
                    <c:choose>
                        <c:when test="${!empty item.game.coverURI}">
                            <img style="width: 15%;" src="${item.game.coverURI}" alt="Brak zdjęcia"/>
                        </c:when>
                        <c:otherwise>
                            Brak zdjęcia
                        </c:otherwise>
                    </c:choose>
                </td>
                <td>
                    <u><a class="btn btn-md btn-block"
                          href="/gameList/${item.game.id}">${item.game.name}</a></u>
                </td>
                <td>
                        ${item.quantity}
                </td>
                <td>
                        ${item.game.price} PLN
                </td>
                <td>
                        ${item.game.price * item.quantity} PLN
                </td>
            </tr>
        </c:forEach>
        <tr>
            <td colspan="5">
                <p>Cena łączna zamówienia: ${order.totalValue} PLN</p>
            </td>
        </tr>
        <security:authorize access="hasRole('EMPLOYEE')">
            <c:if test="${order.status.id == 1}">
                <tr>
                    <td colspan="5">
                        <a href="/employeePanel/orderManagement/accept/${order.id}"
                           class="btn btn-success btn-lg btn-block btn-raised">Akceptuj</a>
                    </td>
                </tr>
            </c:if>
        </security:authorize>
        </tbody>
    </table>

</div>
</body>
</html>

<c:import url="shared/footer.jsp"/>
