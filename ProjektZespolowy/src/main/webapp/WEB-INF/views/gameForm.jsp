<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<html>
<c:import url="shared/header.jsp">
    <c:param name="pageName" value="gameForm"/>
</c:import>
<head>
    <title>Formularz gry</title>
    <script>
        var maxLength = ${maxShortDescriptionLength};

        function enforceMaxLength(obj) {
            if (obj.value.length > maxLength) {
                obj.value = obj.value.substring(0, maxLength);
            }
            document.getElementById("descriptionShortLabel").innerText = "Krótki opis " + obj.value.length + "/" + maxLength;
        }
    </script>
</head>
<body>
<div class="container text-center">
    <div class="order-md-1">
        <h2 class="mb-3">Formularz gry</h2>

        <form:form modelAttribute="game" action="/employeePanel/gameManagement/form">
            <h4>Informacje podstawowe</h4>
            <form:input path="id" cssClass="invisible disabled"/>
            <div class="mb-3">
                <label for="name" class="required">Nazwa</label><br>
                <form:errors path="name" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:input path="name" cssClass="form-control" cssErrorClass="form-control is-invalid"
                                placeholder="Nazwa gry" required="true"/>
                </div>
            </div>
            <div class="mb-3">
                <label id="descriptionShortLabel" for="descriptionShort" class="required">Krótki opis</label><br>
                <form:errors path="descriptionShort" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:textarea onkeyup="enforceMaxLength(this)" rows="5" path="descriptionShort"
                                   cssClass="form-control"
                                   cssErrorClass="form-control is-invalid"
                                   placeholder="Krótki opis" required="true"/>
                </div>
            </div>
            <div class="mb-3">
                <label for="descriptionLong" class="required">Pozostały opis</label><br>
                <form:errors path="descriptionLong" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:textarea rows="15" path="descriptionLong" cssClass="form-control"
                                   cssErrorClass="form-control is-invalid"
                                   placeholder="Pozostały opis (inny niż krótki)" required="true"/>
                </div>
            </div>
            <div class="mb-3">
                <label for="price" class="required">Cena</label><br>
                <form:errors path="price" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:input path="price" cssClass="form-control" cssErrorClass="form-control is-invalid"
                                placeholder="Cena gry" required="true"/>
                </div>
            </div>
            <div class="mb-3">
                <label for="publisher" class="required">Wydawca</label><br>
                <form:errors path="publisher" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:input path="publisher" cssClass="form-control" cssErrorClass="form-control is-invalid"
                                placeholder="Nazwa wydawcy" required="true"/>
                </div>
            </div>
            <div class="mb-3">
                <label for="producer" class="required">Producent</label><br>
                <form:errors path="producer" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:input path="producer" cssClass="form-control" cssErrorClass="form-control is-invalid"
                                placeholder="Nazwa producenta" required="true"/>
                </div>
            </div>
            <div class="mb-3">
                <label for="releaseDate" class="required">Data wydania</label><br>
                <form:errors path="releaseDate" cssClass="alert-danger"/>
                <div class="input-group">
                    <form:input path="releaseDate" cssClass="form-control" cssErrorClass="form-control is-invalid"
                                placeholder="Data wydania" required="true" type="date"/>
                </div>
            </div>
            <div class="mb-3">
                <label for="gameCategories">Kategorie gry</label><br>
                <form:errors path="gameCategories" cssClass="alert-danger"/>
                <div class="list-group">
                    <form:checkboxes path="gameCategories" items="${gameCategories}"
                                     element="div class='form-check' style='left:25px;'" cssClass="form-check-input"
                                     itemLabel="name" itemValue="id"/>
                </div>
            </div>
            <hr>
            <h4>Zdjęcia</h4>
            <div class="mb-3">
                <label class="required">Okładka gry</label><br>
                <form:input path="coverURI" cssClass="form-control" cssErrorClass="form-control is-invalid"
                            placeholder="Adres do okładki" required="true"/>
            </div>
            <div class="mb-3">
                <label>Zdjęcia do galerii</label><br>
                <form:input path="image1" cssClass="form-control" cssErrorClass="form-control is-invalid"
                            placeholder="Pierwsze zdjęcie"/>
            </div>
            <div class="mb-3">
                <form:input path="image2" cssClass="form-control" cssErrorClass="form-control is-invalid"
                            placeholder="Drugie zdjęcie"/>
            </div>
            <div class="mb-3">
                <form:input path="image3" cssClass="form-control" cssErrorClass="form-control is-invalid"
                            placeholder="Trzecie zdjęcie"/>
            </div>

                <div class="mb-3">
                    <label>Ilość gier (0 gdy pole zostanie puste)</label><br>
                    <div class="input-group">
                        <input name="quantity" class="mb-3 form-control" placeholder="0 sztuk" type="number" min="0" value="${quantity}"/>
                    </div>
                </div>

            <button type="submit" class="btn btn-success btn-md btn-block btn-raised">Zapisz</button>
        </form:form>

    </div>
</div>
</body>
<c:import url="shared/footer.jsp"/>
</html>
