<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:import url="shared/header.jsp">
    <c:param name="pageName" value="gameCategoriesManagement"/>
</c:import>

<html>
<head>
    <title>Zarządzanie kategoriami</title>
</head>
<body>

<div class="container text-center">

    <c:choose>
        <c:when test="${!empty gameCategories.content}">
            <div class="col-md-7 ml-auto mr-auto">
                <table class="table table-hover table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nazwa kategorii</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${gameCategories.content}" var="gameCategory">
                        <tr>
                            <th scope="row">
                                    ${gameCategory.id}
                            </th>
                            <td>
                                <form method="GET"
                                      action="/adminPanel/gameCategoriesManagement/save/${gameCategory.id}">
                                    <input class="invisible disabled" name="id" value="${gameCategory.id}"/>
                                    <div class="mb-3">
                                        <div class="input-group">
                                            <input class="form-control col-sm-7" name="name"
                                                   value="${gameCategory.name}" required="true"/>
                                            <button type="submit"
                                                    class="btn btn-warning btn-sm btn-block col-sm-3 ml-auto btn-raised">
                                                Edytuj
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
                <hr>
                <form method="GET"
                      action="/adminPanel/gameCategoriesManagement/save">
                    <div class="mb-3">
                        <div class="input-group">
                            <input class="form-control col-sm-5 ml-auto" name="name"
                                   placeholder="Nowa kategoria" required="true"/>
                            <button type="submit"
                                    class="btn btn-primary btn-sm btn-block col-sm-3 ml-auto mr-auto btn-raised">Dodaj
                            </button>
                        </div>
                    </div>
                </form>
            </div>

            <c:set var="page" value="${gameCategories}" scope="request"/>
            <c:set var="mainUrl" value="/adminPanel/gameCategoriesManagement" scope="request"/>
            <c:import url="shared/pagination.jsp"/>
        </c:when>
        <c:otherwise>
            <h1>Brak danych</h1>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>

<c:import url="shared/footer.jsp"/>
