package com.example.projekt.exceptions;

//rzucany jeżeli nie znaleziono gry w bazie
public class GameNotFoundException extends Exception
{
    public GameNotFoundException ()
    {
        super ( "Gra nie została znaleziona" );
    }
}
