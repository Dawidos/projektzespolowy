package com.example.projekt.exceptions;

//rzucany przy próbie kupna większej ilości gier niż jest w sklepie
public class GameInStoreQuantityEqualsZeroException extends Exception
{
    public GameInStoreQuantityEqualsZeroException ()
    {
        super ( "Twój koszyk został zaktualizowany" );
    }

    public GameInStoreQuantityEqualsZeroException ( String message )
    {
        super ( message );
    }
}
