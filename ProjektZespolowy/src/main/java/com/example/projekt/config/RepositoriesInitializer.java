package com.example.projekt.config;

import com.example.projekt.models.*;
import com.example.projekt.repositories.*;
import com.example.projekt.services.StoreService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

@Configuration
public class RepositoriesInitializer
{
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final GameCategoryRepository gameCategoryRepository;
    private final GameRepository gameRepository;
    private final StoreService storeService;
    private final PasswordEncoder passwordEncoder;
    private final StatusRepository statusRepository;


    @Autowired
    public RepositoriesInitializer ( RoleRepository roleRepository, UserRepository userRepository, GameCategoryRepository gameCategoryRepository, GameRepository gameRepository, StoreService storeService, PasswordEncoder passwordEncoder, StatusRepository statusRepository )
    {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.gameCategoryRepository = gameCategoryRepository;
        this.gameRepository = gameRepository;
        this.storeService = storeService;
        this.passwordEncoder = passwordEncoder;
        this.statusRepository = statusRepository;
    }


    @Bean
    InitializingBean init ()
    {
        return () -> {
            if ( this.roleRepository.findAll ()
                    .isEmpty () )
            {

                Role roleUser = this.roleRepository.save ( new Role ( Role.Types.ROLE_USER ) );
                Role roleEmployee = this.roleRepository.save ( new Role ( Role.Types.ROLE_EMPLOYEE ) );
                Role roleAdmin = this.roleRepository.save ( new Role ( Role.Types.ROLE_ADMIN ) );


                User admin = new User ( "admin", true );
                admin.setRoles ( new HashSet<> ( Arrays.asList ( roleAdmin, roleEmployee, roleUser ) ) );
                admin.setPassword ( this.passwordEncoder.encode ( "admin" ) );
                admin.setPasswordConfirm ( admin.getPassword () );
                admin.setCity ( "Warszawa" );
                admin.setPostCode ( "00-001" );
                admin.setStreet ( "Piękna" );
                admin.setHouseNumber ( "5" );
                admin.setEmail ( "test46114@gmail.com" );
                admin.setName ( "Admin" );
                admin.setSurname ( "Admin" );
                admin.setPhoneNumber ( "500100100" );

                User employee = new User ( "employee", true );
                employee.setRoles ( new HashSet<> ( Arrays.asList ( roleEmployee, roleUser ) ) );
                employee.setPassword ( this.passwordEncoder.encode ( "employee" ) );
                employee.setPasswordConfirm ( employee.getPassword () );
                employee.setCity ( "Siedlce" );
                employee.setPostCode ( "08-100" );
                employee.setStreet ( "Normalna" );
                employee.setHouseNumber ( "10" );
                employee.setApartmentNumber ( "3" );
                employee.setEmail ( "employee@game_store.pl" );
                employee.setName ( "Employee" );
                employee.setSurname ( "Employee" );
                employee.setPhoneNumber ( "500100200" );

                User user = new User ( "user", true );
                user.setRoles ( new HashSet<> ( Collections.singletonList ( roleUser ) ) );
                user.setPassword ( this.passwordEncoder.encode ( "user" ) );
                user.setPasswordConfirm ( user.getPassword () );
                user.setCity ( "Warszawa" );
                user.setPostCode ( "00-002" );
                user.setStreet ( "Dziwna" );
                user.setHouseNumber ( "2" );
                user.setEmail ( "user@email.pl" );
                user.setName ( "User" );
                user.setSurname ( "User" );
                user.setPhoneNumber ( "505050505" );


                this.userRepository.save ( admin );
                this.userRepository.save ( employee );
                this.userRepository.save ( user );
            }

            if ( this.gameCategoryRepository.findAll ()
                    .isEmpty () )
            {

                GameCategory action = this.gameCategoryRepository.save ( new GameCategory ( 1, "akcji" ) );
                GameCategory rpg = this.gameCategoryRepository.save ( new GameCategory ( 2, "RPG" ) );
                GameCategory adventure = this.gameCategoryRepository.save ( new GameCategory ( 3, "przygodowe" ) );
                GameCategory tpp = this.gameCategoryRepository.save ( new GameCategory ( 4, "TPP" ) );
                GameCategory sandbox = this.gameCategoryRepository.save ( new GameCategory ( 5, "sandbox" ) );
                GameCategory fantasy = this.gameCategoryRepository.save ( new GameCategory ( 6, "fantasy" ) );
                GameCategory fpp = this.gameCategoryRepository.save ( new GameCategory ( 7, "FPP" ) );
                GameCategory horror = this.gameCategoryRepository.save ( new GameCategory ( 8, "horror" ) );
                GameCategory survival = this.gameCategoryRepository.save ( new GameCategory ( 9, "przetrwanie" ) );


                Game game1 = this.gameRepository.save ( new Game ( 1L,
                        "Wiedźmin 3: Dziki Gon", "Gra action RPG, stanowiąca trzecią część przygód Geralta z Rivii. Podobnie jak we wcześniejszych odsłonach cyklu, Wiedźmin 3: Dziki Gon bazuje na motywach twórczości literackiej Andrzeja Sapkowskiego, jednak nie jest bezpośrednią adaptacją żadnej z jego książek.",
                        "Wiedźmin 3: Dziki Gon (The Witcher 3: Wild Hunt) na komputery osobiste to trzecia odsłona popularnej serii gier RPG akcji opartej na prozie Andrzeja Sapkowskiego. Tytuł wyprodukowało studio CD Projekt RED, czyli zespół odpowiedzialny również za dwie poprzednie części – Wiedźmina z 2007 roku i Wiedźmina 2: Zabójcy Królów z 2011 roku. Produkcja w wersji na PC-ty nie odbiega zawartością od konsolowych wydań, ale użytkownicy „blaszaków” otrzymali kilka niedostępnych na pozostałych platformach sprzętowych elementów. Przede wszystkim mogą oni cieszyć się najwyższą możliwą jakością oprawy graficznej oraz animację na poziomie 60 klatek na sekundę. Oczywiście, by korzystać z tych wszystkich dobrodziejstw, potrzebny jest odpowiednio mocny komputer. Drugim elementem, którego nie znajdziemy na konsolach są modyfikacje. Studio CD Projekt RED udostępniło w sierpniu 2015 roku zainteresowanym osobom oficjalne narzędzia moderskie, umożliwiające m.in. dowolne zmienianie obiektów, statystyk i nie tylko. Zaowocowało to m.in. tymi modami.",
                        new BigDecimal ( 139.90 ),
                        "http://1.bp.blogspot.com/-2aVzsOQk870/U5HZ2BxkzRI/AAAAAAAABA8/hRe09KQ6Vvo/s1600/4YWu0T6.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/521451313.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/521450392.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/521449519.jpg",
                        "CDP",
                        "CD Projekt RED",
                        new Date ( 115, 4, 19 ),
                        Arrays.asList ( action, rpg, tpp, fantasy, sandbox ) ) );

                Game game2 = this.gameRepository.save ( new Game ( 2L,
                        "The Elder Scrolls V: Skyrim",
                        "Zrealizowana z dużym rozmachem gra action-RPG, będąca piątą częścią popularnego cyklu The Elder Scrolls. Produkcja studia Bethesda Softworks pozwala graczom ponownie przenieść się do fantastycznego świata Nirn.",
                        "The Elder Scrolls V: Skyrim to kolejna część serii cRPG autorstwa zespołu Bethesda Softworks. Ponownie odwiedzamy w niej kontynent Tamriel, a fabuła tym razem obraca się wokół powrotu do tej krainy pradawnej rasy smoków.\n" +
                                "\n" +
                                "Akcja toczy się 200 lat po wydarzeniach opowiedzianych w grze Oblivion. Gracze wcielają się w jednego z ostatnich bohaterów parających się profesją dovahkiina, czyli łowcy smoków. Do niedawna wydawało się, że potężne monstra są już tylko wspomnieniem, ale pewnego dnia pokryte łuskami bestie znów zaczynają pojawiać się na niebie, a przepowiednia zwiastuje nadejście Alduina, boga zniszczenia, który przyjmuje postać smoka. Walcząc z tymi groźnymi potworami przejmujemy ich dusze oraz zdolności, czyli tak zwane Krzyki. Za ich pomocą możemy spowolnić czas, odepchnąć przeciwników niewidzialną energią, a nawet wezwać na pomoc sprzymierzonego smoka. Wiele tego typu umiejętności otrzymujemy po odnalezieniu pradawnych słów, ukrytych w nordyckich podziemiach.\n" +
                                "\n" +
                                "Zabawę zaczynamy od stworzenia naszego wirtualnego alter-ego. Do wyboru jest dziesięć różnorodnych ras, brak natomiast podziału na klasy. Wraz z postępami w zabawie nasza postać staje się coraz potężniejsza. Po przejściu na kolejny poziom można samodzielnie zwiększyć jedną z trzech podstawowych cech: magię, wytrzymałość i zdrowie. Autorzy gry zupełnie przebudowali system umiejętności – zostały one podzielone na czytelne grupy, np. bronie jednoręczne, czy dwuręczne. Każda z takich kategorii zawiera konkretne zdolności i ataki. Zmiany zaszły także w systemie magii – zaklęcia ze szkoły Mistycyzmu zostały przeniesione do innych grup.\n" +
                                "\n" +
                                "W Skyrim pojawia się nowy system walki, który jest bardziej dynamiczny i jednocześnie wymaga myślenia taktycznego. Każdej z dłoni bohatera można teraz przypisać broń, tarczę lub zaklęcie. W ten sposób tworzymy wiele kombinacji, łącząc np. czar ofensywny w jednej ręce z mieczem w drugiej. Bezpośrednie starcia polegają już nie tylko na bezmyślnym wciskaniu klawiszy, ale odpowiednim manewrowaniu bohaterem, unikaniu lub parowaniu ciosów i powalaniu przeciwników. Herosi mogą z łatwością stracić równowagę i wystawić się na niebezpieczne uderzenia wrogów, którzy teraz są znacznie bardziej żywotni i groźni. Postać gracza potrafi też wyprowadzać efektowne ciosy wykańczające.\n" +
                                "\n" +
                                "Kolejna odsłona The Elder Scrolls jeszcze raz próbuje stworzyć wrażenie, że przemierzamy prawdziwy świat, zaludniony przez żywych mieszkańców. Decyzje gracza mają teraz poważniejsze konsekwencje, związane z ciekawszym zachowaniem innych postaci. Kiedy przykładowo wyrzucimy sztylet na środku miasta, to być może ktoś nam go odniesie lub wręcz przeciwnie - ukradnie. Zmianom uległy także dialogi, które stały się znacznie bardziej naturalne. Gdy znudzi nam się ratowanie świata możemy odpocząć przy bardziej przyziemnych czynnościach. The Elder Scrolls V: Skyrim pozwala zbierać plony z ziemi, wykuwać bronie w kuźni, gotować, wydobywać surowce w kopalniach, a nawet produkować pancerze.\n" +
                                "\n" +
                                "The Elder Scrolls V: Skyrim oferuje możliwość zwiedzania olbrzymiego wirtualnego świata, w którym znajdziemy m.in. pięć dużych miast i setki podziemi. W celu ułatwienia poruszania się po tak dużym terenie autorzy wprowadzili opcję szybkiej podróży pomiędzy lokacjami. Ponownie zastosowano system automatycznego dostosowania siły przeciwników do umiejętności naszego śmiałka, choć jednocześnie upewniono się, że nie będzie on tak inwazyjny jak w Oblivionie. Usprawniony silnik gry dobiera zadania pod kątem naszych dotychczasowych dokonań i siły postaci. Przykładowo, gdy kobieta prosi nas o odnalezienie porwanej córki system sprawdza, które podziemia już odwiedziliśmy, a następnie umieszcza dziewczynkę tam, gdzie dotąd nie zawitaliśmy, jednocześnie dodając takich przeciwników, by stanowili oni wyzwanie dla naszego bohatera.\n" +
                                "\n" +
                                "Gra korzysta z udoskonalonego silnika graficznego znanego z The Elder Scrolls IV: Oblivion. Znacznie poprawiono wygląd postaci oraz ich mimikę i gestykulację, dodano dynamiczne efekty pogodowe (m.in. śnieg) i symulację wiatru, który teraz realistycznie porusza gałęziami drzew. Gra oferuje opcję wyłączenia wszystkich elementów interfejsu z ekranu, pozwalając tym samym na głębsze zanurzenie się w wirtualny świat.",
                        new BigDecimal ( 159.99 ),
                        "https://www.gry-online.pl/galeria/gry13/1119867468.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/248707578.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/248706062.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/248702343.jpg",
                        "Bethesda Softworks",
                        "Bethesda Softworks",
                        new Date ( 111, 11, 11 ),
                        Arrays.asList ( rpg, fpp, tpp, fantasy, sandbox, action ) ) );

                Game game3 = this.gameRepository.save ( new Game ( 3L,
                        "Resident Evil 2",
                        "Pełnoprawny remake gry Resident Evil 2, która pojawiła się w 1998 roku na PlayStation. Resident Evil 2 Remake opracowany został przez Capcom z myślą o nowoczesnych platformach sprzętowych, bazując na oryginalnej fabule oraz nieco zmodyfikowanej mechanice pierwowzoru.",
                        "Resident Evil 2 Remake to pełnoprawny remake drugiej części kultowej serii survival horrorów firmy Capcom, która ukazała się pierwotnie w 1998 roku na konsoli PlayStation, ale doczekała się także konwersji na komputery PC oraz konsole Dreamcast, Nintendo 64 i GameCube. Projektem odświeżenia legendarnej produkcji zajęło się główne studio deweloperskie Capcomu R&D Division 1, pod wodzą producenta Yoshiaki Hirabayashiego, który wcześniej pracował między innymi nad wydanym na początku 2015 roku Resident Evil HD.",
                        new BigDecimal ( 249.00 ),
                        "https://www.gry-online.pl/galeria/gry13/534672750.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/446356000.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/446351765.jpg",
                        "https://images9.gry-online.pl/galeria/galeria_duze3/446347328.jpg",
                        "Capcom",
                        "Capcom",
                        new Date ( 119, 0, 25 ),
                        Arrays.asList ( action, tpp, horror, survival, adventure ) ) );

                this.storeService.save ( new Store ( 1L, game1, 100 ) );
                this.storeService.save ( new Store ( 2L, game2, 100 ) );
                this.storeService.save ( new Store ( 3L, game3, 100 ) );
            }

            if ( this.statusRepository.findAll ()
                    .isEmpty () )
            {

                this.statusRepository.save ( new Status ( 1, "oczekuje na realizację" ) );
                this.statusRepository.save ( new Status ( 2, "zrealizowane" ) );
                this.statusRepository.save ( new Status ( 3, "anulowane" ) );
            }

        };
    }
}