package com.example.projekt.repositories;

import com.example.projekt.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository< Game, Long >
{
}
