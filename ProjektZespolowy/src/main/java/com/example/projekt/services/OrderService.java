package com.example.projekt.services;

import com.example.projekt.models.*;
import com.example.projekt.repositories.OrderRepository;
import com.example.projekt.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;

@Service
public class OrderService
{
    private final OrderRepository orderRepository;
    private final StatusRepository statusRepository;
    private final HttpSession session;
    private final StoreService storeService;

    //wstrzykiwanie przez konstruktor
    @Autowired
    public OrderService ( OrderRepository orderRepository, StatusRepository statusRepository, HttpSession session, StoreService storeService )
    {
        this.orderRepository = orderRepository;
        this.statusRepository = statusRepository;
        this.session = session;
        this.storeService = storeService;
    }

    //Znajdź zamówienia złożone przez zalogowanego użytkownika (stronnicowane)
    public Page< Order > findUserOrders ( Pageable pageable )
    {
        User user = ( User ) this.session.getAttribute ( "user" ); //Pobranie z sesji informacji o użytkowniku

        //zwrócenie zamówień dla konkretnego użytkownika (po jego loginie)
        return this.orderRepository.findAllOrdersByUsername ( user.getUsername (), pageable );
    }

    //To co wyżej ale zwraca tylko zamówienia o podanym statusie (wszystki, oczekujące, zatwierdzone lub anulowane) (stronnicowane)
    public Page< Order > findUserOrdersUsingFilter ( int status, Pageable pageable )
    {
        User user = ( User ) this.session.getAttribute ( "user" );

        return this.orderRepository.findAllOrdersByUsernameUsingFilter ( status, user.getUsername (), pageable );
    }

    //Zwraca wszystkie zamówienia (każdego usera) o podanym statusie (wszystki, oczekujące, zatwierdzone lub anulowane) (stronnicowane)
    public Page< Order > findAllOrdersUsingFilter ( int status, Pageable pageable )
    {
        return this.orderRepository.findAllOrdersForEmployeeUsingFilter ( status, pageable );
    }

    //Anulowanie zamówienia
    public void cancelTheOrder ( Order order )
    {
        //Sprawdza, czy zamówienie jest oczekujące oraz czy należy do aktualnie zalogowanego użytkownika
        if ( order.getStatus ()
                .getId () == 1 && ( ( User ) this.session.getAttribute ( "user" ) ).getUsername ()
                .equalsIgnoreCase ( order.getUser ()
                        .getUsername () ) )
        {//jeśli tak to
            Status status = this.statusRepository.findById ( 3 )
                    .orElse ( null );//pobranie statusu o id 3 (anulowane), jak nie znajdzie to zwraca null

            if ( status != null ) //jeśli znaleziono status w bazie to
            {
                order.setStatus ( status ); //zmień status dla zamówienia na nowy

                for ( OrderDetail o : order.getOrderDetails () ) //Pętla po wszystkich grach w zamówieniu (bo jak anulujesz to musisz zwiększyć ich ilość w składzie)
                {
                    Store store = this.storeService.findById ( o.getGame ()
                            .getId () ); //pobranie składu według id (id składu jest takie jak id gry)

                    if ( store != null ) //jak znaleziono skład
                    {
                        store.setQuantity ( store.getQuantity () + o.getQuantity () ); //dodaj do istniejącej ilości ilość egzemplarzy gry z zamówienia
                        this.storeService.save ( store ); //zapisanie nowych danych składu
                    }
                }

                this.orderRepository.saveAndFlush ( order ); //zapisanie nowych danych zamówienia (zmieniony status)
            }
        }
    }

    //akceptacja zamówienia przez pracownika
    public void acceptOrder ( Order order )
    {
        if ( order.getStatus ()
                .getId () == 1 ) //jeśli status to oczekujący
        {
            Status status = this.statusRepository.findById ( 2 )
                    .orElse ( null ); //pobranie z bazy statusu zatwierdzony, jak nie znajdzie to null

            if ( status != null ) //jeśli znaleziono taki status to
            {
                order.setStatus ( status ); //przypisz do zamówienia nowy status
                this.orderRepository.saveAndFlush ( order ); //zapisz nowe dane zamówienia
            }
        }
    }

    //zwróć wszystkie zamówienia dla pracownika (stronnicowane)
    public Page< Order > findAllOrders ( Pageable pageable )
    {
        return this.orderRepository.findAllOrdersForEmployee ( pageable );
    }
}
