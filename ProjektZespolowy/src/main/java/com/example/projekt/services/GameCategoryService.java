package com.example.projekt.services;

import com.example.projekt.models.GameCategory;
import com.example.projekt.repositories.GameCategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

//Serwis zarządzający kategoriami dla gier

@Service
public class GameCategoryService
{
    private final GameCategoryRepository gameCategoryRepository;

    //wstrzyknięcie przez konstruktor
    @Autowired
    public GameCategoryService ( GameCategoryRepository gameCategoryRepository )
    {
        this.gameCategoryRepository = gameCategoryRepository;
    }

    //Pobranie wszystkich kategorii gier (stronnicowanie)
    public Page< GameCategory > findAll ( Pageable pageable )
    {
        return this.gameCategoryRepository.findAllByOrderById ( pageable );
    }

    //Zapisanie danych o nowej lub edytowanej kategorii
    public void save ( GameCategory gameCategory )
    {
        this.gameCategoryRepository.save ( gameCategory );
    }
}
