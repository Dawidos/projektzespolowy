package com.example.projekt.services;

import com.example.projekt.config.ProfileNames;
import com.example.projekt.models.Role;
import com.example.projekt.repositories.RoleRepository;
import com.example.projekt.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Service ( value = "userDetailsService" )
@Profile ( ProfileNames.DATABASE )
public class UserServiceImpl implements UserService
{
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;
    private final HttpSession session;

    //wstrzykiwanie przez konstruktor
    @Autowired
    public UserServiceImpl ( UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder, HttpSession session )
    {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
        this.session = session;
    }

    //zwraca usera po loginie (konwertuje go na Obiekt UserDetails)
    @Override
    @Transactional ( readOnly = true )
    public UserDetails loadUserByUsername ( String username ) throws UsernameNotFoundException
    {
        com.example.projekt.models.User user = this.userRepository.findUserByUsername ( username ); //Pobranie danych usera z bazy (po loginie)

        if ( user == null )//jeśli user nie istnieje
        {
            throw new UsernameNotFoundException ( username ); //wyrzuca wyjątek
        }

        this.session.setAttribute ( "user", user ); //zapisz do sesji dane usera

        return convertToUserDetails ( user ); //przekonwertowanie usera na UserDetails i zwrócenie go
    }

    //Metoda konwertuje na UserDetails
    private UserDetails convertToUserDetails ( com.example.projekt.models.User user )
    {
        //ustawienie, że konto usera jest aktwowane
        user.setEnabled ( true );
        //utworzenie Seta do przechowywania ról usera
        Set< GrantedAuthority > grantedAuthorities = new HashSet<> ();
        for ( Role role : user.getRoles () )//pętla po wszystkich rolach usera
        {
            grantedAuthorities.add ( new SimpleGrantedAuthority ( role.getType ()
                    .toString () ) );//dodaj rolę do Seta (po zmianie tylu na GrantedAuthority
        }

        return new User ( user.getUsername (), user.getPassword (), grantedAuthorities );//zwróć nowego usera (po przekonwertowaniu)
    }

    //zapisz nowo zarejestrowanego usera do bazy
    @Override
    public void save ( com.example.projekt.models.User user )
    {
        //ustaw nowemu userowi role USER
        user.setRoles ( new HashSet<> ( Collections.singletonList ( this.roleRepository.findRoleByType ( Role.Types.ROLE_USER ) ) ) );
        //kodowanie hasła
        user.setPassword ( this.passwordEncoder.encode ( user.getPassword () ) );
        //wyzerowanie potwierdzenia hasła
        user.setPasswordConfirm ( null );

        this.userRepository.saveAndFlush ( user );//zapisz usera do bazy
    }

    //sprawdza czy login jest dostępny
    @Override
    public boolean isLoginAvailable ( String username )
    {
        return this.userRepository.findUserByUsername ( username ) == null;//zwraca true jeżeli nie znajdzie w bazie usera o podanym loginie
    }
}
