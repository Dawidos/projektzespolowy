package com.example.projekt.services;

import com.example.projekt.exceptions.GameInStoreQuantityEqualsZeroException;
import com.example.projekt.models.*;
import com.example.projekt.repositories.OrderDetailRepository;
import com.example.projekt.repositories.OrderRepository;
import com.example.projekt.repositories.StatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.*;

@Service
public class ShoppingCartService
{
    private final OrderRepository orderRepository;
    private final OrderDetailRepository orderDetailRepository;
    private final HttpSession session;
    private final StatusRepository statusRepository;
    private final StoreService storeService;

    //wstrzykiwanie przez konstruktor
    @Autowired
    public ShoppingCartService ( OrderRepository orderRepository, OrderDetailRepository orderDetailRepository, HttpSession session, StatusRepository statusRepository, StoreService storeService )
    {
        this.orderRepository = orderRepository;
        this.orderDetailRepository = orderDetailRepository;
        this.session = session;
        this.statusRepository = statusRepository;
        this.storeService = storeService;
    }

    //Pobranie koszyka z sesji
    public List< OrderDetail > getCart ()
    {
        List< OrderDetail > cart = ( List< OrderDetail > ) this.session.getAttribute ( "cart" );

        if ( cart == null ) //jeżeli w sesji nie ma koszyka (cart równe null)
        {
            cart = new ArrayList<> (); //przypisanie do zmiennej nowej listy (żeby nie było wyjątku spowodowanego przez wartość NULL)
        }

        return cart; //zwrócenie koszyka
    }

    //dodanie gry do koszyka
    public void addGameToCart ( Store store )
    {
        List< OrderDetail > cart = ( List< OrderDetail > ) session.getAttribute ( "cart" ); //pobranie koszyka z sesji

        if ( cart == null )//jeżeli nie ma koszyka
        {
            cart = new ArrayList<> (); //utwórz nową listę dla koszyka
            cart.add ( new OrderDetail ( store.getGame (), 1 ) );//dodanie do listy koszyka gry w ilości 1
            session.setAttribute ( "cart", cart );
        }
        else //jeżeli jest koszyk
        {
            int index = exist ( store.getGame () ); //pobranie indeksu pod którym jest już dodawana gra w koszyku (bo może już być taka gra w koszyku np. gdy chcemy kupić 2 takie same)

            if ( index == -1 ) //jeżeli indeks to -1, to znaczy, że takiej gry nie ma w koszyku
            {
                cart.add ( new OrderDetail ( store.getGame (), 1 ) ); //utworzenie obiektu OrderDetail, w którym jest dodana gra oraz ilość
            }
            else //jeżeli taka gra już jest w koszyku
            {
                int quantity = cart.get ( index )
                        .getQuantity () + 1; //pobranie z koszyka z pod indeksu, w którym już jest dodawana gra ilości tej gry i dodanie do tej ilości 1
                cart.get ( index )
                        .setQuantity ( quantity ); //ustawienie nowej ilości gry w koszyku
            }
            session.setAttribute ( "cart", cart ); //zapisanie koszyku w sesji pod atrybutem o nazwie 'cart'
        }
    }

    //zwraca indeks pod którym znajduje się podana gra w koszyku lub -1 jeżeli takiej gry w nim nie ma
    private int exist ( Game game )
    {
        List< OrderDetail > cart = getCart (); //pobranie koszyka

        for ( int i = 0; i < cart.size (); ++i ) //pętla po całym koszyku
        {
            if ( cart.get ( i )
                    .getGame ()
                    .getId ()
                    .equals ( game.getId () ) )//sprawdzenie, czy gra z koszyka, która jest pod indeksem 'i' ma takie id jak szukana gra
            {//jeżeli tak to
                return i; //zwraca 'i' (indeks pod którym jest gra)
            }
        }

        return -1; //jak nie znajdzie to zwraca -1
    }

    //usuwą grę z koszyka
    public void removeGameFromCart ( Game game )
    {
        List< OrderDetail > cart = getCart (); //pobranie koszyka
        int index = exist ( game ); //pobranie indeksu pod którym jest gra w koszyku

        if ( index != -1 ) //jeżeli znalleziono w koszyku taką grę (indeks różny od -1)
        {
            cart.remove ( index );//usuń z koszyka grę, która jest pod indeksem 'index'
            session.setAttribute ( "cart", cart );//zapisz koszyk do sesji
        }
    }

    //robi to samo co metoda powyżej ale usuwa kilka gier na raz
    public void removeGamesFromCart ( List< Game > games )
    {
        List< OrderDetail > cart = getCart (); //pobranie koszyka
        for ( Game game : games ) //pętla po liście gier podanej jako parametr metody
        {
            int index = exist ( game ); //znalezienie w koszyku indeksu dla usuwanej gry

            if ( index != -1 ) //jeżeli gra jest w koszyku
            {
                cart.remove ( index ); //usuń grę z koszyka
            }
        }
        session.setAttribute ( "cart", cart ); //po wszystkim zapisz koszyk do sesji
    }

    //zmniejsza ilość egzemplarzy gry w koszyku o 1
    public void decreaseGameQuantityInCart ( Game game )
    {
        List< OrderDetail > cart = getCart ();//pobranie koszyka
        int index = exist ( game ); //pobranie indeksu z koszyka

        if ( index != -1 ) //jeżeli gra jest w koszyku
        {
            int quantity = cart.get ( index )
                    .getQuantity () - 1;//pobierz z koszyka ilość egzemplarzy gry i zmniejsz tą ilość o 1

            if ( quantity <= 0 ) //jeżeli ilość jest mniejsza lub równa 0 (gry już nie powinno być w koszyku
            {
                cart.remove ( index ); //usunięcie z koszyka gry która jest pod podanym indexem
            }
            else//w przeciwnym razie
            {
                cart.get ( index )
                        .setQuantity ( quantity ); //ustal nową ilość dla podanej gry (ilość mniejsza o 1)
            }

            session.setAttribute ( "cart", cart ); //zapisz koszyk do sesji
        }
    }

    //Kupuje gry z koszyka
    @Transactional
    public void buyGamesFromCart ( User user ) throws GameInStoreQuantityEqualsZeroException //wyjątek rzucony jeżeli chcesz coś kupić ale nie ma tyle gier w sklepie
    {
        Order order = new Order ( user ); //tworzenie nowego zamówienia dla usera

        List< OrderDetail > cart = getCart ();//pobranie z sesji koszyka

        boolean throwException = false; //zmienna od której zależy, czy rzucić wyjątek
        HashMap< Store, Integer > stores = new HashMap<> (); //utworzenie nowej hashmapy (klucz to skład, wartość to ilość do kupienia)
        List< Game > toDelete = new ArrayList<> (); //lista gier do usunięcia
        for ( OrderDetail o : cart )//pętla po wszystkich elementach koszyka
        {
            Store store = this.storeService.findById ( o.getGame ()
                    .getId () );//pobranie składu dla podanej gry (według id)
            if ( store != null ) //jeżeli taki skład został znaleziony to
            {
                Integer quantity = o.getQuantity (); //pobranie ilości danej gry (ile chcesz kupić)
                if ( quantity > store.getQuantity () ) //jeżeli chcesz kupić więcej gier nież w jest sklepie
                {
                    if ( store.getQuantity () == 0 ) //jeżeli w składzie jest 0 gier (nie ma gry)
                    {
                        toDelete.add ( o.getGame () );//dodaj grę do listy z grami do usunięcia
                    }
                    else//inaczej
                    {
                        int index = exist ( o.getGame () ); //pobranie indeksu pod którym jest gra w koszyku
                        quantity = store.getQuantity (); //pobranie ilości tej gry ze składu (ile jest wszystkich egzemplarzy w sklepie)
                        cart.get ( index )
                                .setQuantity ( quantity );//ustaw ilość egzemplarzy gry w koszyku na ilość egzemplarzy w sklepie (chciałeś kupić więcej nież było w sklepie, więc trzeba zmienić ilość na tyle ile jest wszystkich, bo jest ich mniej niż chciałeś)

                        stores.put ( store, store.getQuantity () - quantity );//dodaj do hashmapy skład i ilośćegzemplarzy gry po odjęciu tej ilości, którą chcesz kupić
                    }
                    throwException = true; //jeżeli dojdzie tutaj to ustawi się zmienna na true, więc dalej w kodzie zostanie rzucony wyjątek (to się stanie tylko w sytuacji gdy chcesz zamówić więcej gier niż jest w sklepie)
                }
                else //inaczej
                {
                    stores.put ( store, store.getQuantity () - quantity ); //dodaj do hashmapy skład z ilością egzemplarzy pomniejszoną o ilość którą chcesz kupić
                }
            }
        }

        removeGamesFromCart ( toDelete ); //usuń z koszyka wszystkie gry które są na liście 'toDelete'
        this.session.setAttribute ( "cart", cart ); //zapisz nowy koszyk do sesji (po usunięciu tych gier których nie ma w sklepie)

        if ( throwException )//jeżeli ta zmienna ma wartość true
        {
            throw new GameInStoreQuantityEqualsZeroException (); //to trzeba rzucić wyjątek (metoda zostanie przerwana i nastąpi przekierowanie na inną stronę
        }

        //Jeżeli nie będzie wyjątku
        for ( Store store : stores.keySet () )//pętla po całej hashmapie
        {
            store.setQuantity ( stores.get ( store ) );//ustawia dla składu nową ilość gier (po odjęciu tych co trzeba kupić)
        }

        this.storeService.saveAll ( stores.keySet () );//zapisanie wszystkich składów w bazie (aktualizacja danych)

        this.orderDetailRepository.saveAll ( cart );//zapisanie wszystkich produktów z koszyka do bazy

        order.setOrderDetails ( new HashSet<> ( cart ) ); //dodanie do zamówienia wszystkich elementów z koszyka
        order.setDate ( new Date () );//ustaw datę zamówienia na aktualną
        order.setTotalValue ( getTotalValue () );//ustal wartość całkowitą zamówienia
        order.setStatus ( this.statusRepository.findById ( 1 )
                .orElse ( null ) );//ustaw status zamówienia na oczekujący (oczekujący ma id równe 1)

        this.orderRepository.saveAndFlush ( order );//zapisz zamówienie do bazy


        this.session.removeAttribute ( "cart" ); //usuń koszyk z sesji
    }

    //zwraca cenę całkowitą za koszyk
    public BigDecimal getTotalValue ()
    {
        List< OrderDetail > cart = getCart ();//pobranie koszyka

        BigDecimal totalValue = new BigDecimal ( 0.00 );//cena wszystkiego na początku równa się 0.00

        for ( OrderDetail orderDetail : cart ) //pętla po wszystkich elementach koszyka
        {
            totalValue = totalValue.add ( orderDetail.getGame ()
                    .getPrice ()
                    .multiply ( new BigDecimal ( orderDetail.getQuantity () ) ) );//pomnożenie ceny gry razy ilość egzemplarzy i dadanie do ceny całkowitej
        }

        return totalValue;//zwrócenie ceny całkowitej
    }
}
