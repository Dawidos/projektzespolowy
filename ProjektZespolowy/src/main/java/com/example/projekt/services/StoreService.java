package com.example.projekt.services;

import com.example.projekt.models.Store;
import com.example.projekt.repositories.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Set;

@Service
public class StoreService
{
    private final StoreRepository storeRepository;

    //wstrzykiwanie przez konstruktor
    @Autowired
    public StoreService ( StoreRepository storeRepository )
    {
        this.storeRepository = storeRepository;
    }

    //Zwraca skład o podanym id
    public Store findById ( Long id )
    {
        return this.storeRepository.findById ( id )
                .orElse ( null );//zwraca skład jeśli jest w bazie, inaczej null
    }

    //zapisuje skład do bazy
    public void save ( Store store )
    {
        this.storeRepository.saveAndFlush ( store );
    }

    //Zwraca wszystkie składy z grami według podanych kryteriów (phrase, priceMin, priceMax) (stronnicowane)
    public Page< Store > findAllGamesInStoreUsingFilter ( String phrase, BigDecimal priceMin, BigDecimal priceMax, Pageable pageable )
    {
        return this.storeRepository.findAllGamesInStoreUsingFilter ( "%" + phrase + "%", priceMin, priceMax, pageable );
    }

    //zapisuje wszystkie składy któe poda się w liście elementów (w Set)
    void saveAll ( Set< Store > stores )
    {
        this.storeRepository.saveAll ( stores );
    }
}
