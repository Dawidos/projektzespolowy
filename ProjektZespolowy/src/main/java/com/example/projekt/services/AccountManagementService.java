package com.example.projekt.services;

import com.example.projekt.models.Role;
import com.example.projekt.models.User;
import com.example.projekt.repositories.RoleRepository;
import com.example.projekt.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

//Serwis zarządzający kontami

@Service
public class AccountManagementService
{
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    //Wstrzykiwanie przez konstruktor
    @Autowired
    public AccountManagementService ( UserRepository userRepository, RoleRepository roleRepository )
    {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    //Zwraca wszystkich użytkowników (z obsługą stronnicowania)
    public Page< User > findAll ( Pageable pageable )
    {
        return this.userRepository.findAll ( pageable );
    }

    //Zwraca użytkowników spełniających wymagania zawarte w searchPhrase (stronnicowanie)
    public Page< User > findAllUsersUsingFilter ( String searchPhrase, Pageable pageable )
    {
        return this.userRepository.findAllUsersUsingFilter ( "%" + searchPhrase + "%", pageable );
    }

    //Zmiana usera na pracownika
    public void hireUserAsEmployee ( User user )
    {
        //pobranie z repozytorium roli pracownika i roli usera oraz ustawienie tych ról dla zalogowanego użytkownika
        user.setRoles ( new HashSet<> ( Arrays.asList ( this.roleRepository.findRoleByType ( Role.Types.ROLE_EMPLOYEE ), this.roleRepository.findRoleByType ( Role.Types.ROLE_USER ) ) ) );

        //Zapisz użytkownika w bazie (aktualizacja danych użytkownika)
        this.userRepository.save ( user );
    }

    //Zmiana pracownika na usera
    public void fireUser ( User user )
    {
        //pobranie z repozytorium roli usera oraz ustawienie jej dla zalogowanego użytkownika
        user.setRoles ( new HashSet<> ( Collections.singletonList ( this.roleRepository.findRoleByType ( Role.Types.ROLE_USER ) ) ) );

        //zapisanie użytkownika
        this.userRepository.save ( user );
    }

    //Zwraca użytkownika z podanym id
    public User findUserById ( Long id )
    {
        return this.userRepository.findById ( id )
                .orElse ( null ); //zwróć usera z podanym id. Jak nie znajdzie takiego to zwróci null
    }

    //Aktualizuj dane o userze (formularz zmiany adresu)
    public void updateUser ( User user, User userForm ) //user to dane usera, userform to dane wprowadzone do formularza
    {
        user.setCity ( userForm.getCity () );
        user.setPostCode ( userForm.getPostCode () );
        user.setStreet ( userForm.getStreet () );
        user.setHouseNumber ( userForm.getHouseNumber () );
        user.setApartmentNumber ( userForm.getApartmentNumber () );

        //Zapis do bazy danych nowych informacji o userze
        this.userRepository.saveAndFlush ( user );
    }
}
