package com.example.projekt.services;

import com.example.projekt.models.Order;
import com.example.projekt.models.OrderDetail;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;

@Service
public class PdfService
{
    private final String pdfRootPath = "/pdf";


    public String createPdf ( Order order, HttpServletRequest request )
    {
        File pdfRootDir = new File ( request.getServletContext ()
                .getRealPath ( this.pdfRootPath ) );

        if ( !pdfRootDir.exists () )
        {
            pdfRootDir.mkdirs ();
        }
        String pdfPath = pdfRootDir.getAbsolutePath () + File.separator;
        String pdfName = order.getUser ()
                .getUsername () + "_" + order.getDate ()
                .getTime () + ".pdf";

        try
        {
            FileOutputStream file = new FileOutputStream ( pdfPath + pdfName );
            Document document = new Document ( PageSize.A4 );
            PdfWriter writer = PdfWriter.getInstance ( document, file );

            Calendar calendar = Calendar.getInstance ();
            calendar.setTime ( order.getDate () );
            document.open ();

            StringBuilder stringBuilder = new StringBuilder ( "" +
                    "<html><head><style>td {padding: 10px;}</style></head>" +
                    "   <body style='font-family:arial;'>" +
                    "<H1>Zamówienie numer: " + order.getId () + "</H1>" +
                    "<h2>Imię i nazwisko: " + order.getUser ()
                    .getName () + " " + order.getUser ()
                    .getSurname () + "</h2>" +

                    "<h3>Data zamówienia: " + calendar.get ( Calendar.YEAR ) + "-" + ( ( calendar.get ( Calendar.MONTH ) + 1 ) < 10 ? ( "0" + ( calendar.get ( Calendar.MONTH ) + 1 ) ) : ( calendar.get ( Calendar.MONTH ) + 1 ) ) + "-" + ( ( calendar.get ( Calendar.DAY_OF_MONTH ) ) < 10 ? ( "0" + calendar.get ( Calendar.DAY_OF_MONTH ) ) : ( calendar.get ( Calendar.DAY_OF_MONTH ) ) ) + " " + ( calendar.get ( Calendar.HOUR_OF_DAY ) < 10 ? ( "0" + calendar.get ( Calendar.HOUR_OF_DAY ) ) : ( calendar.get ( Calendar.HOUR_OF_DAY ) ) ) + ":" + ( calendar.get ( Calendar.MINUTE ) < 10 ? ( "0" + calendar.get ( Calendar.MINUTE ) ) : calendar.get ( Calendar.MINUTE ) ) + ":" + ( calendar.get ( Calendar.SECOND ) < 10 ? ( "0" + calendar.get ( Calendar.SECOND ) ) : ( calendar.get ( Calendar.SECOND ) ) ) + "</h3>" +
                    "       <table border='1'>" +
                    "           <tr>" +
                    "               <th>Okładka</th>" +
                    "               <th>Nazwa</th>" +
                    "               <th>Ilość</th>" +
                    "               <th>Cena gry</th>" +
                    "               <th>Cena łączna</th>" +
                    "           </tr>" );

            for ( OrderDetail orderDetail : order.getOrderDetails () )
            {
                stringBuilder.append ( "<tr>" + "<td>" + "<img style='width: 50%;' src='" )
                        .append ( orderDetail.getGame ()
                                .getCoverURI () )
                        .append ( "' alt='Brak zdjęcia'/>" )
                        .append ( "</td>" )
                        .append ( "<td>" )
                        .append ( orderDetail.getGame ()
                                .getName () )
                        .append ( "</td>" )
                        .append ( "<td>" )
                        .append ( orderDetail.getQuantity () )
                        .append ( "</td>" )
                        .append ( "<td>" )
                        .append ( orderDetail.getGame ()
                                .getPrice () )
                        .append ( " PLN" )
                        .append ( "</td>" )
                        .append ( "<td>" )
                        .append ( orderDetail.getGame ()
                                .getPrice ()
                                .multiply ( new BigDecimal ( orderDetail.getQuantity () ) )
                                .setScale ( 2, RoundingMode.HALF_UP )
                                .doubleValue () )
                        .append ( " PLN" )
                        .append ( "</td>" )
                        .append ( "</tr>" );
            }
            stringBuilder.append ( "<tr>" + "<td colspan='5'>" + "<p>Cena łączna zamówienia: " )
                    .append ( order.getTotalValue () )
                    .append ( " PLN</p>" )
                    .append ( "</td>" )
                    .append ( "</tr>" )
                    .append ( "</table>" )
                    .append ( "</body></html>" );
            InputStream inputStream = new ByteArrayInputStream ( stringBuilder.toString ()
                    .getBytes () );
            XMLWorkerHelper.getInstance ()
                    .parseXHtml ( writer, document, inputStream );
            document.close ();
            file.close ();
        }
        catch ( IOException | DocumentException e )
        {
            e.printStackTrace ();
        }

        return pdfName;
    }
}