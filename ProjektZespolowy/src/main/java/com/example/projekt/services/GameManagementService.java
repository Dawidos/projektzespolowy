package com.example.projekt.services;

import com.example.projekt.models.Game;
import com.example.projekt.models.GameCategory;
import com.example.projekt.models.Store;
import com.example.projekt.repositories.GameCategoryRepository;
import com.example.projekt.repositories.GameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

@Service
public class GameManagementService
{
    private final GameRepository gameRepository;
    private final StoreService storeService;
    private final GameCategoryRepository gameCategoryRepository;

    //wstrzykiwanie przez konstruktor
    @Autowired
    public GameManagementService ( GameRepository gameRepository, StoreService storeService, GameCategoryRepository gameCategoryRepository )
    {
        this.gameRepository = gameRepository;
        this.storeService = storeService;
        this.gameCategoryRepository = gameCategoryRepository;
    }

    //Pabranie wszystkich gier spełniających wymagania zawarte w phrase, priceMin, priceMax (stronnicowanie)
    public Page< Store > findAllGamesUsingFilter ( String phrase, BigDecimal priceMin, BigDecimal priceMax, Pageable pageable )
    {
        return this.storeService.findAllGamesInStoreUsingFilter ( phrase, priceMin, priceMax, pageable );
    }

    //Zapisz nową lub edytowaną grę
    public void save ( Game game, Integer quantity ) //game - dane o grze, quantity - ilość gier w sklepie
    {
        this.gameRepository.save ( game ); //zapisz do bazy grę (jeśli nowa to tworzy nowy rekord, jeśli edytowana to nadpisuje stary rekord)
        Store store = storeService.findById ( game.getId () ); //zwraca skład w którym jest gra i jej ilość (w Store jest info o grze i ilości egzemplarzy w sklepie - taki jakby magazyn)
        if ( store == null ) //jeżeli nie ma takiego składu (to znaczy, że gra jest nowa (nie jest edytowana tylko całkiem nowa)
        {
            store = new Store (); //utworzenie nowego składu
        }
        store.setGame ( game ); //ustawienie dla składu jaką grę przechowuje
        store.setQuantity ( ( quantity != null && quantity > 0 ? quantity : 0 ) ); //ustawienie ilości egzemplarzy w składzie (jeżeli quantity jest różne od null i jest większe od 0 to wstawia wartość quantity, w innym przypadku wstawia 0)
        this.storeService.save ( store ); //zapisanie składu
    }

    //Zwraca wszystkie kategorie gier jako listę (posortowane według id)
    public List< GameCategory > findAllGameCategories ()
    {
        return this.gameCategoryRepository.findAllByOrderById ();
    }
}
