package com.example.projekt.models;

import com.example.projekt.annotation.AvailableUsername;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.validator.constraints.CreditCardNumber;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Set;

@Entity
@Table ( name = "users" )
@Getter
@Setter
@NoArgsConstructor
public class User
{
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private Long id;

    @NotBlank
    @AvailableUsername
    private String username;

    @NotBlank
    private String password;

    @Transient //nie będzie zapisane do bazy
    private String passwordConfirm;

    private boolean enabled = true;

    @ManyToMany ( fetch = FetchType.EAGER )
    @JoinTable ( name = "users_roles", joinColumns = @JoinColumn ( name = "user_id" ), inverseJoinColumns = @JoinColumn ( name = "role_id" ) )
    private Set< Role > roles;

    @Column ( nullable = false )
    @NotBlank
    private String city;

    @Column ( name = "post_code", nullable = false )
    @NotBlank
    @Pattern ( regexp = "^\\d{2}-\\d{3}$" )//wzór dla wpisania kodu pocztowego (XX-XXX)
    private String postCode;

    @Column ( nullable = false )
    @NotBlank
    private String street;

    @Column ( name = "house_number", nullable = false )
    @NotBlank
    @Pattern ( regexp = "^[1-9]\\d*\\w?$" )//wzór do wpisania numeru mieszkania (liczba i opcjonalnie jedna litera)
    private String houseNumber;

    @Column ( name = "apartment_number" )
    @Pattern ( regexp = "^([1-9]\\d*)?$" )//wzór do wpisania numeru budynku (liczba)
    private String apartmentNumber;

    @Column ( nullable = false )
    @NotBlank
    @Email
    private String email;

    @Column ( nullable = false )
    @NotBlank
    private String name;

    @Column ( nullable = false )
    @NotBlank
    private String surname;

    @Column ( name = "phone_number", nullable = false )
    @NotBlank
    //wzór na wpisanie numeru telefonu (można wpisać na kilka sposobów np. 500100100, 12 500 13 13, (+48) 5001123... itp)
    @Pattern ( regexp = "^(?:\\(?\\+?48)?(?:[-\\.\\(\\)\\s]*(\\d)){9}\\)?$" )
    private String phoneNumber;

    public User ( String username )
    {
        this ( username, true );
    }

    public User ( String username, boolean enabled )
    {
        this.username = username;
        this.enabled = enabled;
    }

    //sprawdza czy oba chasła są poprawne
    @AssertTrue
    public boolean isPasswordsEquals ()
    {
        return password == null || passwordConfirm == null || password.equals ( passwordConfirm );
    }

}
