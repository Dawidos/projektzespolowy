package com.example.projekt.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

//Model dla kategorii gry
@Entity
@Table ( name = "game_categories" )
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GameCategory implements Serializable
{
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private Integer id;

    @Column ( nullable = false )
    @NotBlank

    private String name;
}
