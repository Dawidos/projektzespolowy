package com.example.projekt.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

//Model dla ról
@Entity
@Table ( name = "roles" )
@Getter
@Setter
@NoArgsConstructor
public class Role
{
    @Id
    @GeneratedValue ( strategy = GenerationType.IDENTITY )
    private Integer id;

    @Enumerated ( EnumType.STRING )//typy będą wyświetlane jako String (tak jak są napisane w public enum Types na dole)
    private Types type;

    @ManyToMany ( mappedBy = "roles" )
    private Set< User > users;

    public Role ( Types type )
    {
        this.type = type;
    }

    public enum Types
    {
        ROLE_ADMIN,
        ROLE_EMPLOYEE,
        ROLE_USER
    }
}
