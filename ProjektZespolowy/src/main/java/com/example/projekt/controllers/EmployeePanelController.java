package com.example.projekt.controllers;

import com.example.projekt.controllers.commands.GameFilter;
import com.example.projekt.controllers.commands.OrderFilter;
import com.example.projekt.models.*;
import com.example.projekt.repositories.StatusRepository;
import com.example.projekt.services.GameManagementService;
import com.example.projekt.services.OrderService;
import com.example.projekt.services.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.datetime.DateFormatter;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

@Controller
@RequestMapping ( "/employeePanel" )
@SessionAttributes ( { "searchGameEmployeeCommand", "searchOrderEmployeeCommand" } )
public class EmployeePanelController
{
    private final GameManagementService gameManagementService;
    private final OrderService orderService;
    private final StatusRepository statusRepository;
    private final StoreService storeService;

    //wstrzykiwanie przez konstruktor
    @Autowired
    public EmployeePanelController ( GameManagementService gameManagementService, OrderService orderService, StatusRepository statusRepository, StoreService storeService )
    {
        this.gameManagementService = gameManagementService;
        this.orderService = orderService;
        this.statusRepository = statusRepository;
        this.storeService = storeService ;
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @GetMapping ( value = { "/gameManagement/edit/{id}", "/gameManagement/add" } )
    public String displayGameForm ( Model model, @PathVariable ( name = "id", required = false ) Game game )
    {
        if ( game != null ) //jeżeli gra istnieje, czyli chcesz ją edytować
        {
            Store store = this.storeService.findById ( game.getId () );//pobranie składu dla tej gry
            Integer quantity = null;
            if ( store != null ) //jeżeli skład istnieje
            {
                quantity = store.getQuantity (); //pobierz z niego ilość gier
            }
            model.addAttribute ( "quantity", quantity ); //dodaj tą ilość do modelu
        }
        else //jeżeli gra nie istnieje
        {
            game = new Game (); //utwórz nową grę
            model.addAttribute ( "quantity", 0 ); //ustaw ilość na 0 w modelu
        }

        model.addAttribute ( "maxShortDescriptionLength", 300 ); //dodaj do modelu maksymalną długość krótkiego opisu (musi być takie samo jak wartość @Size w modelu Game dla descriptionShort)
        model.addAttribute ( "game", game );//dodaj grę do modelu

        return "gameForm";
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @PostMapping ( value = "/gameManagement/form" )
    public String processGameForm ( @Valid @ModelAttribute ( "game" ) Game game, BindingResult result, @RequestParam ( value = "quantity", required = false ) Integer quantity )
    {
        if ( result.hasErrors () ) //jeżeli są błędy w formularzu to
        {
            return "gameForm"; //wróć do formularza
        }

        this.gameManagementService.save ( game, quantity );//zapisz grę i ilość

        return "redirect:/employeePanel/gameManagement";
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @RequestMapping ( value = "/gameManagement", method = { RequestMethod.GET, RequestMethod.POST } )
    public String displayGames ( Model model, Pageable pageable, @Valid @ModelAttribute ( "searchGameEmployeeCommand" ) GameFilter search )
    {
        Page page = this.gameManagementService.findAllGamesUsingFilter ( search.getPhrase (), search.getPriceMin (), search
                .getPriceMax (), pageable );
        model.addAttribute ( "gameListPage", page );
        return "employeeGameManagement";
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @GetMapping ( value = "/gameManagement", params = { "all" } )
    public String resetGamesFilter ( @ModelAttribute ( "searchGameEmployeeCommand" ) GameFilter search )
    {
        search.clear ();
        return "redirect:/employeePanel/gameManagement";
    }

    @ModelAttribute ( "gameCategories" )
    public List< GameCategory > loadCategories ()
    {
        return this.gameManagementService.findAllGameCategories ();
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @RequestMapping ( value = "/orderManagement", method = { RequestMethod.GET, RequestMethod.POST } )
    public String loadAllOrders ( Model model, Pageable pageable, @Valid @ModelAttribute ( "searchOrderEmployeeCommand" ) OrderFilter search )
    {
        Page page;
        if ( search.getStatus ()
                .getId () != 0 )
        {
            page = this.orderService.findAllOrdersUsingFilter ( search.getStatus ()
                    .getId (), pageable );
        }
        else
        {
            page = this.orderService.findAllOrders ( pageable );
        }
        model.addAttribute ( "orderList", page );

        return "employeeOrderManagement";
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @GetMapping ( value = "/orderManagement", params = { "all" } )
    public String resetOrdersFilter ( @ModelAttribute ( "searchOrderEmployeeCommand" ) OrderFilter search )
    {
        search.clear ();
        return "redirect:/employeePanel/orderManagement";
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @GetMapping ( value = "/orderManagement/accept/{id}" )
    public String acceptOrder ( @PathVariable ( "id" ) Order order )
    {
        this.orderService.acceptOrder ( order );

        return "redirect:/employeePanel/orderManagement";
    }

    @Secured ( "ROLE_EMPLOYEE" )
    @GetMapping ( value = "/orderManagement/{id}" )
    public String displayOrderDetails ( Model model, @PathVariable ( name = "id" ) Order order )
    {
        if ( order == null )
        {
            return "redirect:/employeePanel/orderManagement";
        }
        model.addAttribute ( "order", order );

        return "orderDetails";
    }

    @ModelAttribute ( "statuses" )
    public List< Status > loadStatuses ()
    {
        return this.statusRepository.findAll ();
    }

    @ModelAttribute ( "searchGameEmployeeCommand" )
    public GameFilter getGameSearchForEmployee ()
    {
        return new GameFilter ();
    }

    @ModelAttribute ( "searchOrderEmployeeCommand" )
    public OrderFilter getOrderSearchForEmployee ()
    {
        return new OrderFilter ();
    }

    @InitBinder
    public void initBinder ( WebDataBinder binder )
    {
        binder.addCustomFormatter ( new DateFormatter ( "yyyy-MM-dd" ) );

        DecimalFormat numberFormat = new DecimalFormat ( "#0.00" );
        numberFormat.setMaximumFractionDigits ( 2 );
        numberFormat.setMinimumFractionDigits ( 2 );
        numberFormat.setGroupingUsed ( false );
        CustomNumberEditor priceEditor = new CustomNumberEditor ( BigDecimal.class, numberFormat, true );
        binder.registerCustomEditor ( BigDecimal.class, "minPrice", priceEditor );
        binder.registerCustomEditor ( BigDecimal.class, "maxPrice", priceEditor );
    }
}
