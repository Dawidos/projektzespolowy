package com.example.projekt.controllers.commands;

import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.NumberFormat;

import javax.validation.constraints.PositiveOrZero;
import java.math.BigDecimal;

//Według tego filtru są szukane gry
@Getter
@Setter
public class GameFilter
{
    private String phrase;

    @PositiveOrZero
    @NumberFormat ( style = NumberFormat.Style.CURRENCY )
    private BigDecimal priceMin;

    @PositiveOrZero
    @NumberFormat ( style = NumberFormat.Style.CURRENCY )
    private BigDecimal priceMax;


    public void clear ()
    {
        this.phrase = "";
        this.priceMin = null;
        this.priceMax = null;
    }


    public boolean isEmpty ()
    {
        return ( this.phrase == null || this.phrase.isEmpty () || this.phrase.trim ().isEmpty () ) && this.priceMin == null && this.priceMax == null;
    }


    public String getPhrase ()
    {
        if ( phrase == null )
        {
            this.phrase = "";
        }
        return this.phrase.trim ();
    }
}
