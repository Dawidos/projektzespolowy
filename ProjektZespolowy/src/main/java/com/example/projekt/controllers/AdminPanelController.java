package com.example.projekt.controllers;

import com.example.projekt.controllers.commands.UserFilter;
import com.example.projekt.models.GameCategory;
import com.example.projekt.models.Role;
import com.example.projekt.models.User;
import com.example.projekt.services.AccountManagementService;
import com.example.projekt.services.GameCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
@RequestMapping ( "/adminPanel" )
public class AdminPanelController
{
    private final AccountManagementService accountManagementService;
    private final GameCategoryService gameCategoryService;

    //wstrzykiwanie przez kontroler
    @Autowired
    public AdminPanelController ( AccountManagementService accountManagementService, GameCategoryService gameCategoryService )
    {
        this.accountManagementService = accountManagementService;
        this.gameCategoryService = gameCategoryService;
    }

    @Secured ( "ROLE_ADMIN" )
    @RequestMapping ( value = "/accountManagement", method = { RequestMethod.GET, RequestMethod.POST } )
    public String displayUsers ( Model model, Pageable pageable, @Valid @ModelAttribute ( "searchUser" ) UserFilter search )
    {
        Page page;
        if ( search.isEmpty () )//jeśli nie ma podanych kryteriów wyszukiwania
        {
            page = this.accountManagementService.findAll ( pageable ); //zwróć wszystkich userów
        }
        else //inaczej
        {
            page = this.accountManagementService.findAllUsersUsingFilter ( search.getPhrase (), pageable ); //zwróć według kryteriów
        }
        model.addAttribute ( "userCommand", page );//dodaj userów do modelu (żeby zmienna była widoczna w jsp)

        return "adminUsersManagement";//zwrócenie nazwy widoku jsp
    }

    @Secured ( "ROLE_ADMIN" )
    @GetMapping ( value = "/accountManagement/hire/{id}" )
    public String makeEmployee ( @PathVariable ( name = "id" ) User user )
    {
        //ta pętla sprawdza, czy user którego chcesz zatrudnić jest adminem
        for ( Role role : user.getRoles () )
        {
            if ( role.getType () == Role.Types.ROLE_ADMIN )
            {
                return "redirect:/adminPanel/accountManagement";//jeśli tak to przekierowuje, bo nie można zatrudnić admina
            }
        }
        this.accountManagementService.hireUserAsEmployee ( user );

        return "redirect:/adminPanel/accountManagement";

    }

    @Secured ( "ROLE_ADMIN" )
    @GetMapping ( value = "/accountManagement/fire/{id}" )
    public String makeUser ( @PathVariable ( name = "id" ) User user )
    {
        //To jest to co wyżej ale zapobiega zwolnieniu admina
        for ( Role role : user.getRoles () )
        {
            if ( role.getType () == Role.Types.ROLE_ADMIN )
            {
                return "redirect:/adminPanel/accountManagement";
            }
        }
        this.accountManagementService.fireUser ( user );

        return "redirect:/adminPanel/accountManagement";
    }

    @Secured ( "ROLE_ADMIN" )
    @RequestMapping ( value = "/gameCategoriesManagement", method = { RequestMethod.GET, RequestMethod.POST } )
    public String displayCategories ( Model model, Pageable pageable )
    {
        Page page = this.gameCategoryService.findAll ( pageable );

        model.addAttribute ( "gameCategories", page );

        return "adminCategoriesManagement";
    }

    @Secured ( "ROLE_ADMIN" )
    @GetMapping ( value = { "/gameCategoriesManagement/save/{id}", "/gameCategoriesManagement/save" } )
    public String saveCategories ( @PathVariable ( value = "id", required = false ) GameCategory gameCategory, @RequestParam ( value = "name", required = false ) String name )
    {
        if ( gameCategory == null )
        {
            gameCategory = new GameCategory ();
        }
        gameCategory.setName ( name );

        this.gameCategoryService.save ( gameCategory );
        return "redirect:/adminPanel/gameCategoriesManagement";
    }
}
